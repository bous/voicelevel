# Voice level estimation

This repository provides inference code
for the voice level estimation of our paper
["Analysis and transformation of voice level in singing voice"](https://doi.org/10.1109/ICASSP49357.2023.10095740)
([ArXiv](https://arxiv.org/abs/2204.04006)).

## Installation

### Requirements

For the inference only `tensorflow 2` and `numpy` are required.
To use the helper functions for loading audio files
and calculating mel-spectrograms and visualization,
`librosa` and `matplotlib` are required.
In my environment I have used the versions from the table below,
but most recent versions of these libraries should be compatible.

| Library               | Version |
| -------               | ------- |
| python                |  3.7.12 |
| tensorflow            |  2.6    |
| numpy                 |  1.20.3 |
| matplotlib (optional) |  3.4.3  |
| librosa    (optional) |  0.8.1  |

## Usage

### Command line

Easiest use is through the `voicelevel.py` script:

```sh
  python voicelevel.py example.flac -o example.npy
```

which loads the file `example.flac` estimates the voice level
and stores the result in the numpy array `example.npy`
at a sampling rate of 80 Hz.

Omitting the output file in the command line
plots the voice level instead (matplotlib required).

### Library

#### Load model

To use the voice level inside your own python project
use the `Model` class from the `voicelevel.model` module.

```python
  from voicelevel.model import Model

  estimator = Model.AD2.load()
```

The `Model` class is an enum type
and contains all available model configurations.

The models created by the `.load()` method
expect mel-spectrograms as 4d tensors with the dimensions
`(sample, time, mel_bands=80, 1)`
and produces 4d tensors with the same semantic dimensions:
`(sample, time, 1, 1)`.

#### Calculate input mel-spectrograms

The mel-spectrograms need to be calculated according to the parameters
from the module `voicelevel.mel` (librosa required).

```python
  from voicelevel.mel import load_mel

  mel = load_mel("example.flac")
```

or for more verbose

```python
  from voicelevel.mel import load_audio, generate_mel, mel_params

  audio = load_audio("example.flac", mel_params=mel_params)
  mel = generate_mel(audio, mel_params=mel_params)
```

Note, `load_audio` loads and resamples the audio to 24000 kHz.
This is necessary because the mel-computation method
`generate_mel` expects a signal of this sample rate
(more specifically the sample rate defined by `mel_params.audio_rate`).

If you are using your own mel-spectrogram computation method,
make sure that the input is properly scaled to the range `[-1, 1]`
using the `voicelevel.mel.Scale`
and the same mel-parameters.
Detailed analysis parameters are found in the
`voicelevel.mel.mel_params` object.

#### Summary

Estimating voice level boils down to two steps: 

1. Calculate the mel-spectrogram
2. Invoke the voice level estimator

```python
  from voicelevel.model import Model
  from voicelevel.mel import load_audio, generate_mel, mel_params

  estimator = Model.AD2.load()

  audio = load_audio("example.flac", mel_params=mel_params)
  mel = generate_mel(audio, mel_params=mel_params)
  vl = estimator(mel)
```

## Models

Different models are provided under `models/<model>/`.
I had some trouble exporting the original models `Ad` and `Le`
from the ICASSP paper.
So for the moment I'm only providing an improved model `Ad2`.
If you want to reproduce the results from the original paper
and need access to the original models,
please open an issue or contact Frederik Bous.

### Ad2

Based on the "adaptive recording factor" method **Ad**
with a few improvements and simplifications:

- Ad2 uses the same power contour `n` for target curves and normalization:

  optimize `Enc(mel - n) -> n`

- The power contour is calculated on the linear scale mel-spectrogram
  based on exactly one frame:
  `n = tf.reduce_sum(mel_lin, axis=2)`
