from __future__ import annotations

from pathlib import Path

import tensorflow as tf
from tensorflow.keras import layers as tfl

from voicelevel.mel import frame_energy, mel_params

WEIGHTS = str(Path(__file__).parent / "model")


def load():
    model = create_model()
    model.load_weights(WEIGHTS)
    return model


def create_model():
    input = tf.keras.Input((None, 80, 1))
    output = model_function(input)
    return tf.keras.Model(inputs=input, outputs=output)


def model_function(
    mel,
    filter_sizes=[3, 3, 1, 1, 1, 1, 1, 1, 1],
    n_features=[80, 100, 100, 100, 100, 100, 100, 100, 50],
):
    norm = normalize(mel)
    h = mel - norm

    h = tf.transpose(h, perm=(0, 1, 3, 2))
    for fsize, nf in zip(filter_sizes, n_features):
        h = tfl.Conv2D(nf, (fsize, 1), padding="same")(h)
        h = tf.nn.relu(h)
    h = tfl.Conv2D(1, 1, padding="same")(h)
    h = tf.math.tanh(h)

    return to_db(h)


def normalize(mel, math=tf.math):
    return frame_energy(mel, math=math)


def to_db(x):
    return 50 * x + 50
