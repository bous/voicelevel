import numpy as np
from matplotlib import pyplot as plt

from voicelevel.mel import mel_params


def plot_mel(ax, mel, **kwargs):
    mel = np.squeeze(np.array(mel)).T
    t0 = 0
    t1 = (mel.shape[1] - 1) / mel_params.sample_rate
    ax.imshow(
        mel,
        aspect="auto",
        origin="lower",
        extent=[t0, t1, 0, 80],
        **kwargs,
    )
    ax.set_xlim([t0, t1])
    ax.set_xlabel("Time [s]")
    ax.set_ylabel("Frequency [Hz]")


def plot_vl(ax, x, **kwargs):
    x = np.squeeze(np.array(x))
    t = np.arange(0, x.shape[0]) / mel_params.sample_rate
    ax.plot(t, x, **kwargs)
    ax.set_xlim([t[0], t[-1]])
    ax.set_xlabel("Time [s]")
    ax.set_ylabel("Vocie level [dB]")
