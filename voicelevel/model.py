#!/usr/bin/env python
from __future__ import annotations

from enum import Enum
from pathlib import Path
from typing import TYPE_CHECKING

from voicelevel.utils import expand_dims

if TYPE_CHECKING:
    import tensorflow as tf


def generate_voicelevel(estimator: tf.keras.Model, mel):
    """ """
    if len(mel.shape) == 4:
        mel4d = mel
    elif len(mel.shape) == 3:
        mel4d = expand_dims(mel, [-1])
    elif len(mel.shape) == 2:
        mel4d = expand_dims(mel, [0, -1])
    else:
        raise RuntimeError(
            f"Mel spectrogram must be 2, 3, or 4 dimensional tensor. "
            f"Got tensor of shape {mel.shape} instead"
        )
    voice_level = estimator(mel4d)
    return voice_level


class Model(Enum):
    # AD = "Ad"
    # LE = "Le"
    AD2 = "Ad2"

    def load(self):
        if self == Model.AD2:
            from models.Ad2 import load
        # elif self == Model.AD:
        #     from models.Ad import load
        # elif self == Model.LE:
        #     from models.Le import load
        else:
            raise NotImplementedError(f"Model {self} not found")
        return load()
