from __future__ import annotations

import tensorflow as tf


def expand_dims(x, axes):
    if not axes:
        return x
    a, *xes = axes
    return expand_dims(tf.expand_dims(x, a), xes)
