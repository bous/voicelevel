from __future__ import annotations

from pathlib import Path
from typing import NamedTuple

import numpy as np
import tensorflow as tf
from typing_extensions import Literal

from voicelevel.utils import expand_dims


class Scale:
    def __init__(self, min, max):
        l1 = np.log(min) / 2
        l2 = np.log(max) / 2
        self.a = l1 + l2
        self.b = -l1 + l2

    def __call__(self, data, *, math=np):
        return (math.log(data + 1e-6) - self.a) / self.b

    def invert(self, data, *, math=np):
        return math.exp(self.b * data + self.a)

    @classmethod
    def from_db(cls, min_db, max_db) -> Scale:
        min = 10 ** (min_db / 20)
        max = 10 ** (max_db / 20)
        return cls(min=min, max=max)


class MelParams(NamedTuple):
    sample_rate: float = 80
    audio_rate: float = 24000
    mel_bands: int = 80
    window_size: float = 0.050
    fft_size: int = 0x800
    fmin: float = 0
    fmax: float = 8000
    norm: Literal["slaney", 1] = 1
    scale: Scale = Scale.from_db(-120, -20)


def load_mel(audio_file: str | Path, mel_params=MelParams()):
    audio = load_audio(audio_file, mel_params.audio_rate)
    return generate_mel(audio, mel_params=mel_params)


def generate_mel(audio: np.ndarray, mel_params=MelParams()):
    import librosa

    stft_args = dict(
        frame_length=int(mel_params.window_size * mel_params.audio_rate),
        frame_step=int(mel_params.audio_rate / mel_params.sample_rate),
        fft_length=int(mel_params.fft_size),
    )
    mel_filter_bank = tf.convert_to_tensor(
        librosa.filters.mel(
            sr=mel_params.audio_rate,
            n_fft=mel_params.fft_size,
            n_mels=mel_params.mel_bands,
            fmin=mel_params.fmin,
            fmax=mel_params.fmax,
            htk=False,
            norm=mel_params.norm,
            dtype=np.float32,
        )
    )

    stft_scale = 2 / stft_args["frame_length"]
    stft = tf.signal.stft(audio, **stft_args) * stft_scale
    mspec = tf.tensordot(tf.abs(stft), mel_filter_bank, axes=[-1, 1])
    return expand_dims(mel_params.scale(mspec), [-1])


def frame_energy(mel, math=tf.math):
    mlin = mel_params.scale.invert(mel, math=math)
    l = tf.reduce_sum(mlin, axis=2, keepdims=True)
    norm = mel_params.scale(l, math=math)
    return norm


def load_audio(file: str | Path, audio_rate=None):
    import librosa

    audio, _ = librosa.load(file, sr=audio_rate, mono=False)
    if len(audio.shape) == 1:
        return audio[np.newaxis, :]
    return audio.T


mel_params = MelParams()
