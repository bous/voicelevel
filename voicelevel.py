#!/usr/bin/env python
from __future__ import annotations

from argparse import ArgumentParser, RawDescriptionHelpFormatter
from pathlib import Path
from typing import Optional

import numpy as np

from voicelevel.mel import generate_mel
from voicelevel.model import Model


def main(
    file: Path,
    model: Model,
    out_file: Optional[Path] = None,
):
    estimator = model.load()
    mel = generate_mel(file)
    voice_level = estimator(mel)
    if out_file:
        save_vl(out_file, voice_level)
    else:
        from voicelevel.plot import plot_mel, plot_vl, plt

        _, (ax_mel, ax_vl) = plt.subplots(2, 1)
        plot_mel(ax_mel, mel)
        plot_vl(ax_vl, voice_level)
        plt.show()


def save_vl(file, vl):
    data = np.squeeze(np.array(vl))
    np.save(file, data)


def get_args():
    description = "Estimate voice level"

    parser = ArgumentParser(
        description=description, formatter_class=RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "file",
        help="Input audio file",
        type=Path,
    )
    parser.add_argument(
        "-m",
        "--model",
        type=Model,
        help="Voice level model: 'Ad', 'Le' or 'Ad2'",
        default=Model.AD2,
    )
    parser.add_argument(
        "-o",
        "--out_file",
        type=Path,
        help="Save the result, if not provided, results will be plotted",
    )
    return parser.parse_args()


if __name__ == "__main__":
    main(**vars(get_args()))
