#!/usr/bin/env python
from __future__ import annotations

from pathlib import Path
from typing import Sequence

from voicelevel.mel import frame_energy, generate_mel
from voicelevel.model import Model
from voicelevel.plot import plot_mel, plot_vl, plt

ROOT_DIR = Path(__file__).parent
EXAMPLE_FILE = ROOT_DIR / "example.flac"


def main(*files: Path):
    for file in files:
        estimators = {m: m.load() for m in Model}
        mel = generate_mel(file)

        _, (ax_mel, ax_vl) = plt.subplots(2, 1)
        plot_mel(ax_mel, mel)

        for model, estimator in estimators.items():
            voice_level = estimator(mel)
            plot_vl(ax_vl, voice_level, label=model.value)

        plot_vl(ax_vl, 50 * frame_energy(mel) + 10, label="Frame energy")

        ax_vl.legend()
        ax_vl.set_ylim([0, 80])
    plt.show()


if __name__ == "__main__":
    from sys import argv

    if argv[1:]:
        main(*argv[1:])
    else:
        main(EXAMPLE_FILE)
